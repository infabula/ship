import os
import json

def filter_ship_data_from_string(string_data):
    '''Filters the data from the string and
       converts it to the correct data type. 

       Returns the data as dictionary.
    '''
    #  string data: 'Titanic, 204.0, 3000'
    
    string_items = string_data.split(',')
    
    if len(string_items) == 3:
        ship = {
            'name': string_items[0].strip(),
            'length': float(string_items[1]),
            'max_nb_of_passengers': int(string_items[2])
        }
        return ship
    else:
        raise ValueError("Ship data is incomplete")


def get_ships_from_file(filename=None):
    ships = []
    with open(filename, 'r') as f:
        for index, line in enumerate(f):
            cleaned_ship_data = line.strip()
            try:
                ship = filter_ship_data_from_string(cleaned_ship_data)
                ships.append(ship)
            except Exception as e:
                print("There is an error in line ", index, ".")
    return ships


def pretty_print_ship():
    pass
    

if __name__ == '__main__':    

    file_path = os.path.dirname(__file__)
    filename = 'ships.txt'

    full_path = os.path.join(file_path, filename)

    ships = get_ships_from_file(full_path)
    
    for index, ship in enumerate(ships):
        print("#{} {} {} {}".format(index,
                                    ship['name'],
                                    ship['length'],
                                    ship['max_nb_of_passengers']
                                     ))

    ship_json = json.dumps(ships)
    print(ship_json)
    
