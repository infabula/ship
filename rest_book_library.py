import json
import requests
import sys

#  alternative is: argpars
if len(sys.argv) != 2:
    print("Usage: rest_book_library.py <topic>")
    sys.exit(1)
    
topic = sys.argv[1]
print("Finding books on topic '{}'.".format(topic))

url = 'https://openlibrary.org/subjects/{}.json'.format(topic)
api_session = requests.session()
response = api_session.get(url)

if response.status_code == 200:
    response_content = json.loads(response.text) # convert to python dict

    for book in response_content['works']:
        #for key in book.keys():
        #    print(key)
        author_list = book.get('authors', [])
        authors = ""
        for author in author_list:
            authors += author['name'] + " " 
        print("{} by {}".format(book['title'], authors))
    
else:
    print("Could not fetch data from %s", url)
